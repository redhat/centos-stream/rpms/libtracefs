#!/usr/bin/bash

if rpm -q --quiet libtracefs; then
    :
else
    sudo dnf install -y libtracefs
    if [[ $? != 0 ]]; then
       echo "install of libtracefs failed!"
       exit 1
    fi
fi

echo "The libtracefs is meant to be used by perf, trace-cmd etc. Check installation."
if [[ ! -f /usr/lib64/libtracefs.so.1 ]]; then
    echo "/usr/lib64/libtracefs.so.1 not found!"
    exit 2
fi

echo "Check the trace-cmd works."
if ! rpm -q --quiet trace-cmd; then
    sudo dnf install -y trace-cmd
    if [[ $? != 0 ]]; then
        echo "install trace-cmd failed when libtracefs exist!"
        exit 3
    fi
fi
trace-cmd list || exit 4

exit 0
